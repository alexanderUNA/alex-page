const form = document.getElementById('form-search');

const h1 = document.getElementById('item-was-searched');

const itemForm = 'item-searched';

//  comentario
form.addEventListener('submit', (e) => {
    e.preventDefault();
    // (B1) FORM DATA OBJECT
    var data = new FormData(); 

    // (B2) APPEND FIELDS
    data.append(itemForm, document.getElementById("alex-search").value);

    // (B3) WHATEVER YOU WANT TO DO NEXT
    for (let [k, v] of data.entries()) { console.log(k, v); }

    h1.innerText = data.get(itemForm);

    form.reset();
});